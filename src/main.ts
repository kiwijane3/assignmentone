(window as any).global = window
global = window as any;

import 'core-js/es6/reflect';
import 'core-js/es7/reflect';
import 'pouchdb';

import { AppModule } from './app/app.module';
import PouchDB from 'pouchdb';
import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';



if (environment.production) {
  enableProdMode();
}

console.log(global);

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
