
export class Time {
	// The number of minutes past 12:00AM
	public rawMinutes: number;

	// The hours in 24 hour time.
	get hours() {
		return Math.round(this.rawMinutes / 60);
	}

	// The minutes past the hour
	get minutes() {
		return this.rawMinutes % 60;
	}

	constructor(rawMinutes: number) {
		this.rawMinutes = rawMinutes;
	}

	// Creates a Time by extracting the time from the given date.
	public static fromDate(date: Date): Time {
		return new Time((date.getHours() * 60) + date.getMinutes());
	}

	// Creates a Time based on json data.
	public static fromJson(json: any): Time {
		return new Time(json.rawMinutes);
	}

	public static fromTimeString(string: string): Time {
		let components = string.split(":");
		let hours = Number(components[0]);
		let minutes = Number(components[1]);
		let rawMinutes = (hours * 60) + minutes;
		return new Time(rawMinutes);
	}

	// Creates a json representation of the date for storage.
	public toJSON() {
		return {
			rawMinutes: this.rawMinutes
		}
	}

	// Provides a user-readable string representing the time.
	public toString(): string {
		let minuteString = this.minutes.toString();
		if (this.minutes < 10) {
			minuteString = `0${minuteString}`;
		}
		return `${this.hours}:${minuteString}`;
	}

}

