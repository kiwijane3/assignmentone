import { Injectable } from '@angular/core';
import { onSameDay } from './Date';
import { MoodDate } from './MoodData';
import { MoodDbAdapter } from './MoodDbAdapter';

@Injectable({
	providedIn: 'root'
})
export class MoodManager {

	private history: MoodDate[];

	private adapter: MoodDbAdapter;

	private get today(): MoodDate {
		return this.history[this.history.length - 1];
	}

	public moods = [
		"Happy",
		"Sad",
		"Okay",
		"Anxious",
		"Guilty",
		"Energetic",
		"Tired"
	]

	constructor() {
		this.history = [];
		this.adapter = new MoodDbAdapter();
	}

	private async fetchCurrentDate(): Promise<MoodDate> {
		let recentDate = await this.adapter.getLatestMoodDate();
		console.log("recentDate:");
		console.log(recentDate);
		if (recentDate == null || !onSameDay(recentDate.date, new Date())) {
			console.log("Creating new date");
			let newDate = new MoodDate();
			await this.adapter.addMoodDate(newDate);
			return newDate;
		} else {
			return recentDate;
		}
	}

	// These methods alter moods for today.

	public async toggleMood(mood: string): Promise<void> {
		let today = await this.fetchCurrentDate();
		today.toggleMood(mood);
		await this.adapter.updateMoodDate(today);
	}

	public async moodsToday(): Promise<string[]> {
		let today = await this.fetchCurrentDate();
		return today.activeMoods();
	}

	public async availableMoods(): Promise<string[]> {
		let today = await this.fetchCurrentDate();
		let toRemove: string[] = [];
		if (today.hasMood("Happy")) {
			toRemove.push("Sad");
			toRemove.push("Okay");
		} else if (today.hasMood("Sad")) {
			toRemove.push("Happy");
			toRemove.push("Okay");
		} else if (today.hasMood("Okay")) {
			toRemove.push("Happy");
			toRemove.push("Sad");
		}
		if (today.hasMood("Energetic")) {
			toRemove.push("Tired");
		} else if (today.hasMood("Tired")) {
			toRemove.push("Energetic");
		}
		return this.moods.filter(mood => {
			return !toRemove.includes(mood);
		});
	}

	// This method returns moods for past days.

	public async getRecord(): Promise<MoodRecord[]> {
		let history = await this.adapter.getHistory();
		// An asynchrony issues can cause the creation of a second date for a day. Clean that up here.
		return history.map((moodDate) => {
			return new MoodRecord(moodDate);
		});
	}

}

export class MoodRecord {

	public date: Date;

	public moods: string[];

	public constructor(moodDate: MoodDate) {
		this.date = moodDate.date;
		this.moods = Array.from(moodDate.moods.keys());
	}

}