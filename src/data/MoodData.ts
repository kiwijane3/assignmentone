

// Records the user's mood on a given date.
export class MoodDate {

	// ID for storage: json version of date.
	public _id: string;

	// The date at which this was created. Used to store the day this date represents.
	public date: Date;

	// The moods on the date.
	public moods: Set<string>;

	// Revision id for pouchdb.
	public _rev: string;

	// Constructor; By default, it will create a date for today.
	public constructor(date: Date = null, moods: Set<string> = null, _id: string = null, _rev: string = null) {
		if (date == null) {
			this.date = new Date();
		} else {
			this.date = date;
		}
		if (moods == null) {
			this.moods = new Set<string>();
		} else {
			this.moods = moods;
		}
		if (_id == null) {
			this._id = this.date.toJSON();
		} else {
			this._id = _id;
		}
		this._rev = _rev;
		console.log(this._id);
	}

	public addMood(mood: string) {
		this.moods.add(mood);
	}

	public removeMood(mood: string) {
		this.moods.delete(mood);
	}

	public hasMood(mood: string): boolean {
		return this.moods.has(mood);
	}

	public toggleMood(mood: string) {
		if (this.hasMood(mood)) {
			this.removeMood(mood);
		} else {
			this.addMood(mood);
		}
	}

	// Returns moods for the day as a list.
	public activeMoods(): string[] {
		return Array.from(this.moods.keys());
	}

	// Creates a MoodDate from json.
	public static fromJSON(json: any): MoodDate {
		console.log('MoodDate.fromJSON()');
		console.log(json);
		return new MoodDate(new Date(json.date), new Set<string>(json.moods), json._id, json._rev);
	}

	// Creates a json representation of this isntance for storage.
	public toJSON(): any {
		return {
			_id: this._id,
			date: this.date.toJSON(),
			moods: Array.from(this.moods.keys()),
			_rev: this._rev
		}
	}

}