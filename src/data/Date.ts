export function onSameDay(a: Date, b: Date): boolean {
	console.log(b);
	return a.getFullYear() === b.getFullYear() && a.getMonth() === b.getMonth() && a.getDate() === b.getDate();
}

// Much of this code expects monday to be day 0, but javscript has sunday as day zero. This function transitions between the two.
export function fixDay(day: number) {
	day = day - 1;
	if (day == -1) {
		day = 6;
	}
	return day;
}

export function nameOfMonth(month: number): string {
	if (month == 0) {
		return "January";
	}
	if (month == 1) {
		return "February";
	}
	if (month == 2) {
		return "March";
	}
	if (month == 3) {
		return "April";
	}
	if (month == 4) {
		return "May";
	}
	if (month == 5) {
		return "June"
	}
	if (month == 6) {
		return "July";
	}
	if (month == 7) {
		return "August";
	}
	if (month == 8) {
		return "September";
	}
	if (month == 9) {
		return "October";
	}
	if (month == 10) {
		return "November";
	}
	if (month == 11) {
		return "December";
	}
}

export function nameOfDay(day: number) {
	if (day === 0) {
		return "Monday";
	}
	if (day === 1) {
		return "Tuesday";
	}
	if (day === 2) {
		return "Wednesday";
	}
	if (day === 3) {
		return "Thursday";
	}
	if (day === 4) {
		return "Friday";
	}
	if (day === 5) {
		return "Saturday";
	}
	if (day === 6) {
		return "Sunday";
	}
}