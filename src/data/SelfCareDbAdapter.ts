import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb-browser';
import { SelfCareActivity, SelfCareDate } from './SelfCareData';
import { Time } from './Time';

@Injectable ({
	providedIn: 'root'
})
export class SelfCareDbAdapter {

	public activitiesDatabase: PouchDB.Database;

	public historyDatabase: PouchDB.Database;

	public constructor() {
		this.activitiesDatabase = new PouchDB("Activities");
		this.historyDatabase = new PouchDB("History");
	}

	public addActivity(activity: SelfCareActivity) {
		this.activitiesDatabase.put(activity);
	}

	public async getActivities(): Promise<SelfCareActivity[]> {
		let documents = await this.activitiesDatabase.allDocs({ include_docs: true });
		return documents.rows.flatMap((value) => {
			return SelfCareActivity.fromJson(value.doc);
		});
	}

	public async addSelfCareDate(date: SelfCareDate): Promise<void> {
		await this.historyDatabase.put(date.toJSON());
	}

	public async updateSelfCareDate(date: SelfCareDate): Promise<void> {
		console.log("Updating self care date")
		console.log(date.toJSON());
		await this.historyDatabase.put(date.toJSON());	
	}

	public async getMostRecentSelfCareDate(): Promise<SelfCareDate> {
		let results = await this.historyDatabase.allDocs({ include_docs: true, limit: 1});
		if (results.rows.length > 0) {
			return SelfCareDate.fromJson(results.rows[0].doc);
		} else {
			return null;
		}
	}

}