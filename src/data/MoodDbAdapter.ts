import { MoodDate } from './MoodData';
import PouchDB from 'pouchdb-browser';

export class MoodDbAdapter {

	public moodDatabase: PouchDB.Database;
	
	public constructor() {
		this.moodDatabase = new PouchDB("moodHistory");
	}

	public async addMoodDate(moodDate: MoodDate): Promise<void> {
		await this.moodDatabase.put(moodDate.toJSON());
	}

	public async updateMoodDate(moodDate: MoodDate): Promise<void> {
		await this.moodDatabase.put(moodDate.toJSON());
	}

	public async deleteMoodDate(moodDate: MoodDate): Promise<void> {
		await this.moodDatabase.destroy(moodDate.toJSON());
	}

	public async getLatestMoodDate(): Promise<MoodDate> {
		let results = await this.moodDatabase.allDocs({ include_docs: true, limit: 1});
		if (results.rows.length > 0) {
			return MoodDate.fromJSON(results.rows[0].doc);
		} else {
			return null;
		}
	}

	// Returns past mood dates.
	public async getHistory(): Promise<MoodDate[]> {
		let results = await this.moodDatabase.allDocs({include_docs: true, limit: 1, skip: 1});
		return results.rows.map((row) => {
			return MoodDate.fromJSON(row.doc);
		})
	}

}