import { SelfCareBlock, SelfCareInstance, SelfCareActivity, } from '../../data/SelfCareData'

import { Component, OnInit } from '@angular/core';
import {SelfCareManager} from '../../data/SelfCareManager';
import { nameOfDay } from './../../data/Date';
import { Time } from 'src/data/Time';
import { NavController } from '@ionic/angular';
import { Router, NavigationEnd, Navigation } from '@angular/router';

@Component ({
	selector: 'self-care',
	templateUrl: 'selfCare.page.html'
})
export class SelfCarePage {
	
	private selfCareManager: SelfCareManager;

	public selfCareBlocks: SelfCareBlock[];

	public constructor(selfCareManager: SelfCareManager, router: Router) {
		router.events.subscribe((event) => {
				if (event instanceof NavigationEnd) {
					let navEndEvent = event as NavigationEnd;
					console.log(navEndEvent.url);
					if (event.url == '/tabs/selfCare') {
						console.log("Reloading in response to navigation to root self care component");
						this.reload();
					}
				}
			}
		)
		this.selfCareManager = selfCareManager;
		this.selfCareBlocks = [];
	}

	public printSelfCareManager() {
		console.log(this.selfCareManager);
	}

	public async reload() {
		this.selfCareBlocks = await this.selfCareManager.selfCareBlocks();
	}

	public async setInstanceComplete(instance: SelfCareInstance): Promise<void> {
		if (!instance.completed) {
			await this.selfCareManager.completeActivity(instance);
		}
		this.reload();
	}

	public today(): number {
		return this.selfCareManager.today();
	}

	public nameOfDay(day: number) {
		return nameOfDay(day);
	}

}