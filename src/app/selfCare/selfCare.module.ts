import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SelfCarePage } from './selfCare.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
	RouterModule.forChild([{ path: '', component:  SelfCarePage}])
  ],
  declarations: [SelfCarePage]
})
export class SelfCareModule {}