import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'moods', loadChildren: './moods/moods.module#MoodsPageModule' },
  { path: 'self-care-buddy-introduction', loadChildren: './self-care-buddy-introduction/self-care-buddy-introduction.module#SelfCareBuddyIntroductionPageModule' },
  { path: 'self-care-buddy-added-activity', loadChildren: './self-care-buddy-added-activity/self-care-buddy-added-activity.module#SelfCareBuddyAddedActivityPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
