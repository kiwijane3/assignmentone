import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
		{
			path: 'welcome',
			children: [
				{
					path: '',
					loadChildren: '../welcome/welcome.module#WelcomePageModule'
				}
			]
		},
		{
			path: 'selfCare',
			children: [
				{
					path: '',
					loadChildren: '../selfCare/selfCare.module#SelfCareModule'
				},
				{
					path: 'create',
					children: [
						{
							path: 'introduction',
							loadChildren: '../self-care-buddy-introduction/self-care-buddy-introduction.module#SelfCareBuddyIntroductionPageModule'
						},
						{
							path: 'details',
							loadChildren: '../createSelfCareActivity/createSelfCareActivity.module#CreateSelfCareActivityModule'
						},
						{
							path: 'added',
							loadChildren: '../self-care-buddy-added-activity/self-care-buddy-added-activity.module#SelfCareBuddyAddedActivityPageModule'
						}
					]
				}
			]
		},
		{
			path: 'mood',
			children: [
				{
					path: '',
					loadChildren: '../moods/moods.module#MoodsPageModule'
				}
			]
		},
      	{
        	path: 'tab1',
        	children: [
          		{
            		path: '',
            		loadChildren: '../tab1/tab1.module#Tab1PageModule'
         		}
        	]
      	},
      	{
       		path: 'tab2',
        	children: [
          		{
            		path: '',
            		loadChildren: '../tab2/tab2.module#Tab2PageModule'
          		}
        	]
      	},
      	{
        	path: 'tab3',
        	children: [
          		{
            		path: '',
            		loadChildren: '../tab3/tab3.module#Tab3PageModule'
          		}
        	]
      	},
      	{
        	path: '',
        	redirectTo: '/tabs/welcome',
        	pathMatch: 'full'
      	}
    	]
  	},
  	{
   		path: '',
    	redirectTo: '/tabs',
    	pathMatch: 'full'
  	}
];

@NgModule({
  imports: [
	RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
