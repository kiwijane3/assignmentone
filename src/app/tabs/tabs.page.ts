import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {

	private navController: NavController;

  	constructor(navController: NavController) {
		  this.navController = navController;
  	}

	public ngOnInit() {
		this.navController.navigateForward('/tabs/welcome');
	}

}
