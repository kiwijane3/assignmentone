import { Component } from '@angular/core';
import { MoodManager } from 'src/data/MoodManager';
import { SelfCareManager } from 'src/data/SelfCareManager';
import { Router, NavigationEnd } from '@angular/router';

@Component({
	selector: 'app-welcome',
	templateUrl: 'welcome.page.html',
	styleUrls: ['welcome.page.scss']
})
export class WelcomePage {

	private moodManager: MoodManager;

	private selfCareManager: SelfCareManager;

	private moods: string[]

	constructor(moodManager: MoodManager, selfCareManager: SelfCareManager, router: Router) {
		router.events.subscribe((event) => {
			if (event instanceof NavigationEnd) {
				let navEnd = event as NavigationEnd;
				if (navEnd.urlAfterRedirects == '/tabs/welcome') {
					this.reload();
				}
			}
		})
		this.moodManager = moodManager;
		this.selfCareManager = selfCareManager;
		this.moods = [];
	}

	public async reload(): Promise<void> {
		this.moods = await this.moodManager.moodsToday();
	}

	public getBuddyAsset(): string {
		if (this.moods.includes("Sad")) {
			return "/assets/RaccoonCuddleSad.svg";
		} else {
			return "/assets/RaccoonNeutral.svg";
		}
	}

	public getMoodMessage(): string {
		if (this.moods.includes("Happy")) {
			return "I'm glad you're doing well!";
		} else if (this.moods.includes("Okay")) {
			return "I'm glad you're doing okay.";
		} else if (this.moods.includes("Sad")) {
			return "I'm sorry you're feeling down. I hope you feel better soon.";
		} else {
			return "I hope you're doing okay";
		}
	}

	public getSelfCareReminder() {
		
	}
	
}