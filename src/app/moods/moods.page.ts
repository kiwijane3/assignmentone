import { Component, OnInit } from '@angular/core';
import { MoodManager, MoodRecord } from 'src/data/MoodManager';
import { nameOfDay, nameOfMonth, fixDay } from 'src/data/Date';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-moods',
  templateUrl: './moods.page.html',
  styleUrls: ['./moods.page.scss'],
})
export class MoodsPage {

	private moodManager: MoodManager;

	public activeMoods: string[];

	public availableMoods: string[];

	public history: MoodRecord[];

	constructor(moodManager: MoodManager, router: Router) {
		// Reload data when this page is navigated to.
		router.events.subscribe((event) => {
			if (event instanceof NavigationEnd) {
				let navEnd = event as NavigationEnd;
				if (navEnd.url == '/tabs/mood') {
					this.reload();
				}
			}
		})
		this.moodManager = moodManager;
	}

	// Retrieves relevant data from the moodManager.
	public async reload(): Promise<void> {
		this.activeMoods = await this.moodManager.moodsToday();
		this.availableMoods = await this.moodManager.availableMoods();
		this.history = await this.moodManager.getRecord();
	}

	public hasMood(mood: string): boolean {
		return this.activeMoods.includes(mood);
	}

	public async toggleMood(mood: string): Promise<void> {
		await this.moodManager.toggleMood(mood);
		this.reload();
	}

	// Translates date to human readable-form.
	public displayForDate(date: Date): string {
		// Move the provided sunday-first day to monday-first
		let dayName = nameOfDay(fixDay(date.getDay()));
		let monthName = nameOfMonth(date.getMonth());
		return `${dayName}, ${monthName} ${date.getDate() + 1}`;
	}

	

}
