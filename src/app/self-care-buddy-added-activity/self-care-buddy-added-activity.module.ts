import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SelfCareBuddyAddedActivityPage } from './self-care-buddy-added-activity.page';

const routes: Routes = [
  {
    path: '',
    component: SelfCareBuddyAddedActivityPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SelfCareBuddyAddedActivityPage]
})
export class SelfCareBuddyAddedActivityPageModule {}
