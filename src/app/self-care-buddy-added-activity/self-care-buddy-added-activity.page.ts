import { Component, OnInit } from '@angular/core';

import { SelfCareManager } from 'src/data/SelfCareManager';

@Component({
  selector: 'app-self-care-buddy-added-activity',
  templateUrl: './self-care-buddy-added-activity.page.html',
  styleUrls: ['./self-care-buddy-added-activity.page.scss'],
})
export class SelfCareBuddyAddedActivityPage implements OnInit {
	
	constructor() { 
	}

	ngOnInit() {
	}

}
