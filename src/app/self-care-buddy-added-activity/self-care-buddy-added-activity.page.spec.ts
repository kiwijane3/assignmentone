import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfCareBuddyAddedActivityPage } from './self-care-buddy-added-activity.page';

describe('SelfCareBuddyAddedActivityPage', () => {
  let component: SelfCareBuddyAddedActivityPage;
  let fixture: ComponentFixture<SelfCareBuddyAddedActivityPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfCareBuddyAddedActivityPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfCareBuddyAddedActivityPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
