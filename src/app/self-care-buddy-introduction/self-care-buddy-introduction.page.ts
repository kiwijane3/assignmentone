import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-self-care-buddy-introduction',
  templateUrl: './self-care-buddy-introduction.page.html',
  styleUrls: ['./self-care-buddy-introduction.page.scss'],
})
export class SelfCareBuddyIntroductionPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
