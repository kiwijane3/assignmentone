import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfCareBuddyIntroductionPage } from './self-care-buddy-introduction.page';

describe('SelfCareBuddyIntroductionPage', () => {
  let component: SelfCareBuddyIntroductionPage;
  let fixture: ComponentFixture<SelfCareBuddyIntroductionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfCareBuddyIntroductionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfCareBuddyIntroductionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
