import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SelfCareBuddyIntroductionPage } from './self-care-buddy-introduction.page';

const routes: Routes = [
  {
    path: '',
    component: SelfCareBuddyIntroductionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SelfCareBuddyIntroductionPage]
})
export class SelfCareBuddyIntroductionPageModule {}
