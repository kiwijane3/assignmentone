import { CommonModule } from '@angular/common';
import { CreateSelfCareActivityPage } from './createSelfCareActivity.page';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: CreateSelfCareActivityPage }])
  ],
  declarations: [CreateSelfCareActivityPage]
})
export class CreateSelfCareActivityModule {}