import { Component, Output, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { SelfCareActivity } from 'src/data/SelfCareData';
import { SelfCareManager } from './../../data/SelfCareManager';
import { Time } from './../../data/Time';
import { EventEmitter } from 'events';

@Component ({
	selector: 'createSelfCareActivity',
	templateUrl: 'createSelfCareActivity.page.html'
})
export class CreateSelfCareActivityPage {

	private selfCareManager: SelfCareManager;

	private navController: NavController;

	public name: string;

	public time: Time;

	public days: Boolean[];

	// Whether the state is valid for creating the activity.
	public valid: boolean;

	public constructor(selfCareManager: SelfCareManager, navController: NavController) {
		this.selfCareManager = selfCareManager;
		this.navController = navController;
		this.name = "";
		this.time = Time.fromDate(new Date());
		this.days = [
			false,
			false,
			false,
			false,
			false,
			false,
			false
		];
		this.valid = false;
	}

	handleNameUpdate(event: Event) {
		this.name = event.target['value'];
		this.validate();
	}

	handleTimeUpdate(event: Event) {
		// Parse the time from the time string;
		let timeString = event.target['value'];
		this.time = Time.fromTimeString(timeString);
		this.validate();
	}
	
	createActivity() {
		let activity = new SelfCareActivity(this.name, this.time, this.days);
		this.selfCareManager.addSelfCareActivity(activity);
		this.navController.navigateForward('/tabs/selfCare/create/added');
	}
	
	toggleDay(day: number) {
		let newArray = this.days.slice();
		newArray[day] = !this.days[day];
		this.days = newArray;
		console.log(this.days);
		this.validate();
	}

	validate() {
		console.log("Validating");
		console.log(this.days);
		this.valid = (this.name.trim() != "") && (this.days.includes(true));
		console.log(this.valid);
	}

}